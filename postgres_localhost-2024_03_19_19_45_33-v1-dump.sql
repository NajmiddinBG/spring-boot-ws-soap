toc.dat                                                                                             0000600 0004000 0002000 00000034413 14576322416 0014456 0                                                                                                    ustar 00postgres                        postgres                        0000000 0000000                                                                                                                                                                        PGDMP       "    -                |            sqb_task_db %   14.11 (Ubuntu 14.11-0ubuntu0.22.04.1) %   14.11 (Ubuntu 14.11-0ubuntu0.22.04.1) /    V           0    0    ENCODING    ENCODING        SET client_encoding = 'UTF8';
                      false         W           0    0 
   STDSTRINGS 
   STDSTRINGS     (   SET standard_conforming_strings = 'on';
                      false         X           0    0 
   SEARCHPATH 
   SEARCHPATH     8   SELECT pg_catalog.set_config('search_path', '', false);
                      false         Y           1262    181941    sqb_task_db    DATABASE     `   CREATE DATABASE sqb_task_db WITH TEMPLATE = template0 ENCODING = 'UTF8' LOCALE = 'en_US.UTF-8';
    DROP DATABASE sqb_task_db;
                postgres    false         �            1259    191761 	   auth_user    TABLE     �   CREATE TABLE public.auth_user (
    balance bigint DEFAULT 0 NOT NULL,
    id bigint NOT NULL,
    full_name character varying(255) NOT NULL,
    phone_number character varying(255) NOT NULL,
    wallet_number character varying(255) NOT NULL
);
    DROP TABLE public.auth_user;
       public         heap    postgres    false         �            1259    191760    auth_user_id_seq    SEQUENCE     y   CREATE SEQUENCE public.auth_user_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 '   DROP SEQUENCE public.auth_user_id_seq;
       public          postgres    false    210         Z           0    0    auth_user_id_seq    SEQUENCE OWNED BY     E   ALTER SEQUENCE public.auth_user_id_seq OWNED BY public.auth_user.id;
          public          postgres    false    209         �            1259    191775    client    TABLE     �   CREATE TABLE public.client (
    id bigint NOT NULL,
    password character varying(255) NOT NULL,
    username character varying(255) NOT NULL
);
    DROP TABLE public.client;
       public         heap    postgres    false         �            1259    191774    client_id_seq    SEQUENCE     v   CREATE SEQUENCE public.client_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 $   DROP SEQUENCE public.client_id_seq;
       public          postgres    false    212         [           0    0    client_id_seq    SEQUENCE OWNED BY     ?   ALTER SEQUENCE public.client_id_seq OWNED BY public.client.id;
          public          postgres    false    211         �            1259    191811    databasechangelog    TABLE     Y  CREATE TABLE public.databasechangelog (
    id character varying(255) NOT NULL,
    author character varying(255) NOT NULL,
    filename character varying(255) NOT NULL,
    dateexecuted timestamp without time zone NOT NULL,
    orderexecuted integer NOT NULL,
    exectype character varying(10) NOT NULL,
    md5sum character varying(35),
    description character varying(255),
    comments character varying(255),
    tag character varying(255),
    liquibase character varying(20),
    contexts character varying(255),
    labels character varying(255),
    deployment_id character varying(10)
);
 %   DROP TABLE public.databasechangelog;
       public         heap    postgres    false         �            1259    191806    databasechangeloglock    TABLE     �   CREATE TABLE public.databasechangeloglock (
    id integer NOT NULL,
    locked boolean NOT NULL,
    lockgranted timestamp without time zone,
    lockedby character varying(255)
);
 )   DROP TABLE public.databasechangeloglock;
       public         heap    postgres    false         �            1259    191817    message_template    TABLE     j  CREATE TABLE public.message_template (
    id bigint NOT NULL,
    created_at timestamp without time zone DEFAULT now(),
    created_by bigint NOT NULL,
    deleted boolean NOT NULL,
    updated_at timestamp without time zone,
    content_pattern character varying(255) NOT NULL,
    organization_id bigint NOT NULL,
    title character varying(255) NOT NULL
);
 $   DROP TABLE public.message_template;
       public         heap    postgres    false         �            1259    191823    message_template_id_seq    SEQUENCE     �   CREATE SEQUENCE public.message_template_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 .   DROP SEQUENCE public.message_template_id_seq;
       public          postgres    false    219         \           0    0    message_template_id_seq    SEQUENCE OWNED BY     S   ALTER SEQUENCE public.message_template_id_seq OWNED BY public.message_template.id;
          public          postgres    false    220         �            1259    191786    transaction    TABLE     ~  CREATE TABLE public.transaction (
    amount bigint DEFAULT 0 NOT NULL,
    client_id bigint NOT NULL,
    client_transaction_id bigint NOT NULL,
    created_at timestamp without time zone DEFAULT now(),
    from_user_id bigint NOT NULL,
    id bigint NOT NULL,
    service_id bigint NOT NULL,
    to_user_id bigint NOT NULL,
    updated_at timestamp(6) without time zone,
    status character varying(10),
    generic_params character varying(100000)[],
    CONSTRAINT transaction_status_check CHECK (((status)::text = ANY ((ARRAY['CANCELED'::character varying, 'SUCCESS'::character varying, 'PENDING'::character varying])::text[])))
);
    DROP TABLE public.transaction;
       public         heap    postgres    false         �            1259    191785    transaction_id_seq    SEQUENCE     {   CREATE SEQUENCE public.transaction_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 )   DROP SEQUENCE public.transaction_id_seq;
       public          postgres    false    214         ]           0    0    transaction_id_seq    SEQUENCE OWNED BY     I   ALTER SEQUENCE public.transaction_id_seq OWNED BY public.transaction.id;
          public          postgres    false    213         �            1259    191798    worker_service    TABLE     �   CREATE TABLE public.worker_service (
    id bigint NOT NULL,
    description character varying(255) NOT NULL,
    service_name character varying(255) NOT NULL
);
 "   DROP TABLE public.worker_service;
       public         heap    postgres    false         �            1259    191797    worker_service_id_seq    SEQUENCE     ~   CREATE SEQUENCE public.worker_service_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 ,   DROP SEQUENCE public.worker_service_id_seq;
       public          postgres    false    216         ^           0    0    worker_service_id_seq    SEQUENCE OWNED BY     O   ALTER SEQUENCE public.worker_service_id_seq OWNED BY public.worker_service.id;
          public          postgres    false    215         �           2604    191765    auth_user id    DEFAULT     l   ALTER TABLE ONLY public.auth_user ALTER COLUMN id SET DEFAULT nextval('public.auth_user_id_seq'::regclass);
 ;   ALTER TABLE public.auth_user ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    209    210    210         �           2604    191778 	   client id    DEFAULT     f   ALTER TABLE ONLY public.client ALTER COLUMN id SET DEFAULT nextval('public.client_id_seq'::regclass);
 8   ALTER TABLE public.client ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    212    211    212         �           2604    191824    message_template id    DEFAULT     z   ALTER TABLE ONLY public.message_template ALTER COLUMN id SET DEFAULT nextval('public.message_template_id_seq'::regclass);
 B   ALTER TABLE public.message_template ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    220    219         �           2604    191791    transaction id    DEFAULT     p   ALTER TABLE ONLY public.transaction ALTER COLUMN id SET DEFAULT nextval('public.transaction_id_seq'::regclass);
 =   ALTER TABLE public.transaction ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    214    213    214         �           2604    191801    worker_service id    DEFAULT     v   ALTER TABLE ONLY public.worker_service ALTER COLUMN id SET DEFAULT nextval('public.worker_service_id_seq'::regclass);
 @   ALTER TABLE public.worker_service ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    216    215    216         I          0    191761 	   auth_user 
   TABLE DATA           X   COPY public.auth_user (balance, id, full_name, phone_number, wallet_number) FROM stdin;
    public          postgres    false    210       3401.dat K          0    191775    client 
   TABLE DATA           8   COPY public.client (id, password, username) FROM stdin;
    public          postgres    false    212       3403.dat Q          0    191811    databasechangelog 
   TABLE DATA           �   COPY public.databasechangelog (id, author, filename, dateexecuted, orderexecuted, exectype, md5sum, description, comments, tag, liquibase, contexts, labels, deployment_id) FROM stdin;
    public          postgres    false    218       3409.dat P          0    191806    databasechangeloglock 
   TABLE DATA           R   COPY public.databasechangeloglock (id, locked, lockgranted, lockedby) FROM stdin;
    public          postgres    false    217       3408.dat R          0    191817    message_template 
   TABLE DATA           �   COPY public.message_template (id, created_at, created_by, deleted, updated_at, content_pattern, organization_id, title) FROM stdin;
    public          postgres    false    219       3410.dat M          0    191786    transaction 
   TABLE DATA           �   COPY public.transaction (amount, client_id, client_transaction_id, created_at, from_user_id, id, service_id, to_user_id, updated_at, status, generic_params) FROM stdin;
    public          postgres    false    214       3405.dat O          0    191798    worker_service 
   TABLE DATA           G   COPY public.worker_service (id, description, service_name) FROM stdin;
    public          postgres    false    216       3407.dat _           0    0    auth_user_id_seq    SEQUENCE SET     >   SELECT pg_catalog.setval('public.auth_user_id_seq', 6, true);
          public          postgres    false    209         `           0    0    client_id_seq    SEQUENCE SET     <   SELECT pg_catalog.setval('public.client_id_seq', 10, true);
          public          postgres    false    211         a           0    0    message_template_id_seq    SEQUENCE SET     E   SELECT pg_catalog.setval('public.message_template_id_seq', 4, true);
          public          postgres    false    220         b           0    0    transaction_id_seq    SEQUENCE SET     @   SELECT pg_catalog.setval('public.transaction_id_seq', 6, true);
          public          postgres    false    213         c           0    0    worker_service_id_seq    SEQUENCE SET     C   SELECT pg_catalog.setval('public.worker_service_id_seq', 6, true);
          public          postgres    false    215         �           2606    191771 $   auth_user auth_user_phone_number_key 
   CONSTRAINT     g   ALTER TABLE ONLY public.auth_user
    ADD CONSTRAINT auth_user_phone_number_key UNIQUE (phone_number);
 N   ALTER TABLE ONLY public.auth_user DROP CONSTRAINT auth_user_phone_number_key;
       public            postgres    false    210         �           2606    191769    auth_user auth_user_pkey 
   CONSTRAINT     V   ALTER TABLE ONLY public.auth_user
    ADD CONSTRAINT auth_user_pkey PRIMARY KEY (id);
 B   ALTER TABLE ONLY public.auth_user DROP CONSTRAINT auth_user_pkey;
       public            postgres    false    210         �           2606    191773 %   auth_user auth_user_wallet_number_key 
   CONSTRAINT     i   ALTER TABLE ONLY public.auth_user
    ADD CONSTRAINT auth_user_wallet_number_key UNIQUE (wallet_number);
 O   ALTER TABLE ONLY public.auth_user DROP CONSTRAINT auth_user_wallet_number_key;
       public            postgres    false    210         �           2606    191782    client client_pkey 
   CONSTRAINT     P   ALTER TABLE ONLY public.client
    ADD CONSTRAINT client_pkey PRIMARY KEY (id);
 <   ALTER TABLE ONLY public.client DROP CONSTRAINT client_pkey;
       public            postgres    false    212         �           2606    191784    client client_username_key 
   CONSTRAINT     Y   ALTER TABLE ONLY public.client
    ADD CONSTRAINT client_username_key UNIQUE (username);
 D   ALTER TABLE ONLY public.client DROP CONSTRAINT client_username_key;
       public            postgres    false    212         �           2606    191810 0   databasechangeloglock databasechangeloglock_pkey 
   CONSTRAINT     n   ALTER TABLE ONLY public.databasechangeloglock
    ADD CONSTRAINT databasechangeloglock_pkey PRIMARY KEY (id);
 Z   ALTER TABLE ONLY public.databasechangeloglock DROP CONSTRAINT databasechangeloglock_pkey;
       public            postgres    false    217         �           2606    191826 &   message_template message_template_pkey 
   CONSTRAINT     d   ALTER TABLE ONLY public.message_template
    ADD CONSTRAINT message_template_pkey PRIMARY KEY (id);
 P   ALTER TABLE ONLY public.message_template DROP CONSTRAINT message_template_pkey;
       public            postgres    false    219         �           2606    191796    transaction transaction_pkey 
   CONSTRAINT     Z   ALTER TABLE ONLY public.transaction
    ADD CONSTRAINT transaction_pkey PRIMARY KEY (id);
 F   ALTER TABLE ONLY public.transaction DROP CONSTRAINT transaction_pkey;
       public            postgres    false    214         �           2606    191805 "   worker_service worker_service_pkey 
   CONSTRAINT     `   ALTER TABLE ONLY public.worker_service
    ADD CONSTRAINT worker_service_pkey PRIMARY KEY (id);
 L   ALTER TABLE ONLY public.worker_service DROP CONSTRAINT worker_service_pkey;
       public            postgres    false    216                                                                                                                                                                                                                                                             3401.dat                                                                                            0000600 0004000 0002000 00000000555 14576322416 0014260 0                                                                                                    ustar 00postgres                        postgres                        0000000 0000000                                                                                                                                                                        120000000000	3	Pushkin A.S	+998996673205	9860000000000002
120000000000	4	Jony Doe	+998996673206	9860000000000003
120000000000	5	Javohir Maxmanazarov	+998996673207	9860000000000004
120000000000	6	XXX X.X.	+998996673208	9860000000000005
120000072000	2	Abdulla Oripov	+998996673204	9860000000000001
119999928000	1	Najmiddin Botirov	+998996673203	9860000000000000
\.


                                                                                                                                                   3403.dat                                                                                            0000600 0004000 0002000 00000000125 14576322416 0014253 0                                                                                                    ustar 00postgres                        postgres                        0000000 0000000                                                                                                                                                                        6	psw	SQB-App
7	psw	Uzum-App
8	psw	Ipoteka-App
9	psw	Realpay-App
10	psw	TBC-App
\.


                                                                                                                                                                                                                                                                                                                                                                                                                                           3409.dat                                                                                            0000600 0004000 0002000 00000000316 14576322416 0014263 0                                                                                                    ustar 00postgres                        postgres                        0000000 0000000                                                                                                                                                                        1000	Friday	db/changelog/master.xml	2024-03-19 16:20:00.992197	1	EXECUTED	8:354c3f401a28958cf5a1ed816aefc331	sqlFile path=classpath:db/changelog/insert_init_information.sql		\N	4.20.0	\N	\N	0847200959
\.


                                                                                                                                                                                                                                                                                                                  3408.dat                                                                                            0000600 0004000 0002000 00000000017 14576322416 0014260 0                                                                                                    ustar 00postgres                        postgres                        0000000 0000000                                                                                                                                                                        1	f	\N	\N
\.


                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 3410.dat                                                                                            0000600 0004000 0002000 00000002404 14576322416 0014253 0                                                                                                    ustar 00postgres                        postgres                        0000000 0000000                                                                                                                                                                        1	2023-08-24 01:08:06.934991	-1	f	\N	Hurmatli [FULL_NAME], sizni tug'ilgan kuningiz bilan tabriklaymiz. Keyingi ishlaringizda rivoj tilaymiz	-1	Birthday
2	2023-08-24 01:08:06.934991	-1	f	\N	Hurmatli [STUDENT_NAME], siz [GROUP_NAME] guruhdagi [DATE] shu vaqtdagi darsda yo'qlamadan o'tdingiz	-1	Came to lesson
3	2023-08-24 01:08:06.934991	-1	f	\N	Hurmatli [STUDENT_NAME], siz [GROUP_NAME] guruhdagi [DATE] shu vaqtdagi darsda yo'qlamadan o'tmadingiz, kelajagingiz uchun dars qoldirmaslikka harakat qiling	-1	Did not come to lesson
4	2023-08-24 01:08:06.934991	-1	f	\N	Hurmatli [STUDENT_NAME], sizni [ORGANIZATION_NAME] markazining [GROUP_NAME] nomli guruhi uchun to'lov vaqtingiz yaqin qoldi, iltimos to'lovni vaqtida amalga oshiring!!!	-1	Payment time is near
5	2023-08-24 01:08:06.934991	-1	f	\N	Hurmatli [STUDENT_NAME], sizni [ORGANIZATION_NAME] markazining [GROUP_NAME] nomli guruhi uchun [SUM] miqdorida to'lov amalga oshirdingiz!!!	-1	Payment made
6	2023-08-24 01:08:06.934991	-1	f	\N	Hurmatli [STUDENT_NAME], siz [ORGANIZATION_NAME] markazining [GROUP_NAME] nomli guruhiga qo'shildingiz	-1	Bind to group
7	2023-08-24 01:08:06.934991	-1	f	\N	Hurmatli [STUDENT_NAME], siz [ORGANIZATION_NAME] markazining [GROUP_NAME] nomli guruhidan chiqarib yuborildingiz	-1	Unbind to group
\.


                                                                                                                                                                                                                                                            3405.dat                                                                                            0000600 0004000 0002000 00000002403 14576322416 0014256 0                                                                                                    ustar 00postgres                        postgres                        0000000 0000000                                                                                                                                                                        12000	6	1	2024-03-19 17:01:33.235693	1	1	5	2	2024-03-19 17:01:33.235744	SUCCESS	{"{\\"paramKey\\":\\"customer_id\\",\\"paramValue\\":\\"2\\"}","{\\"paramKey\\":\\"pin\\",\\"paramValue\\":\\"9860000000000000\\"}"}
12000	6	1	2024-03-19 17:01:38.052021	1	2	5	2	2024-03-19 17:01:38.052049	SUCCESS	{"{\\"paramKey\\":\\"customer_id\\",\\"paramValue\\":\\"2\\"}","{\\"paramKey\\":\\"pin\\",\\"paramValue\\":\\"9860000000000000\\"}"}
12000	6	1	2024-03-19 17:01:43.405159	1	3	5	2	2024-03-19 17:01:43.405185	SUCCESS	{"{\\"paramKey\\":\\"customer_id\\",\\"paramValue\\":\\"2\\"}","{\\"paramKey\\":\\"pin\\",\\"paramValue\\":\\"9860000000000000\\"}"}
12000	6	1	2024-03-19 17:01:47.449377	1	4	5	2	2024-03-19 17:01:47.449407	SUCCESS	{"{\\"paramKey\\":\\"customer_id\\",\\"paramValue\\":\\"2\\"}","{\\"paramKey\\":\\"pin\\",\\"paramValue\\":\\"9860000000000000\\"}"}
12000	6	1	2024-03-19 17:01:50.093933	1	5	5	2	2024-03-19 17:01:50.093965	SUCCESS	{"{\\"paramKey\\":\\"customer_id\\",\\"paramValue\\":\\"2\\"}","{\\"paramKey\\":\\"pin\\",\\"paramValue\\":\\"9860000000000000\\"}"}
12000	6	1	2024-03-19 17:01:53.273121	1	6	5	2	2024-03-19 17:01:53.273147	SUCCESS	{"{\\"paramKey\\":\\"customer_id\\",\\"paramValue\\":\\"2\\"}","{\\"paramKey\\":\\"pin\\",\\"paramValue\\":\\"9860000000000000\\"}"}
\.


                                                                                                                                                                                                                                                             3407.dat                                                                                            0000600 0004000 0002000 00000000105 14576322416 0014255 0                                                                                                    ustar 00postgres                        postgres                        0000000 0000000                                                                                                                                                                        4	First service	Uz-1
5	Second service	Uz-2
6	Third service	Uz-3
\.


                                                                                                                                                                                                                                                                                                                                                                                                                                                           restore.sql                                                                                         0000600 0004000 0002000 00000030601 14576322416 0015376 0                                                                                                    ustar 00postgres                        postgres                        0000000 0000000                                                                                                                                                                        --
-- NOTE:
--
-- File paths need to be edited. Search for $$PATH$$ and
-- replace it with the path to the directory containing
-- the extracted data files.
--
--
-- PostgreSQL database dump
--

-- Dumped from database version 14.11 (Ubuntu 14.11-0ubuntu0.22.04.1)
-- Dumped by pg_dump version 14.11 (Ubuntu 14.11-0ubuntu0.22.04.1)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

DROP DATABASE sqb_task_db;
--
-- Name: sqb_task_db; Type: DATABASE; Schema: -; Owner: postgres
--

CREATE DATABASE sqb_task_db WITH TEMPLATE = template0 ENCODING = 'UTF8' LOCALE = 'en_US.UTF-8';


ALTER DATABASE sqb_task_db OWNER TO postgres;

\connect sqb_task_db

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: auth_user; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.auth_user (
    balance bigint DEFAULT 0 NOT NULL,
    id bigint NOT NULL,
    full_name character varying(255) NOT NULL,
    phone_number character varying(255) NOT NULL,
    wallet_number character varying(255) NOT NULL
);


ALTER TABLE public.auth_user OWNER TO postgres;

--
-- Name: auth_user_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.auth_user_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.auth_user_id_seq OWNER TO postgres;

--
-- Name: auth_user_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.auth_user_id_seq OWNED BY public.auth_user.id;


--
-- Name: client; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.client (
    id bigint NOT NULL,
    password character varying(255) NOT NULL,
    username character varying(255) NOT NULL
);


ALTER TABLE public.client OWNER TO postgres;

--
-- Name: client_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.client_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.client_id_seq OWNER TO postgres;

--
-- Name: client_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.client_id_seq OWNED BY public.client.id;


--
-- Name: databasechangelog; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.databasechangelog (
    id character varying(255) NOT NULL,
    author character varying(255) NOT NULL,
    filename character varying(255) NOT NULL,
    dateexecuted timestamp without time zone NOT NULL,
    orderexecuted integer NOT NULL,
    exectype character varying(10) NOT NULL,
    md5sum character varying(35),
    description character varying(255),
    comments character varying(255),
    tag character varying(255),
    liquibase character varying(20),
    contexts character varying(255),
    labels character varying(255),
    deployment_id character varying(10)
);


ALTER TABLE public.databasechangelog OWNER TO postgres;

--
-- Name: databasechangeloglock; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.databasechangeloglock (
    id integer NOT NULL,
    locked boolean NOT NULL,
    lockgranted timestamp without time zone,
    lockedby character varying(255)
);


ALTER TABLE public.databasechangeloglock OWNER TO postgres;

--
-- Name: message_template; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.message_template (
    id bigint NOT NULL,
    created_at timestamp without time zone DEFAULT now(),
    created_by bigint NOT NULL,
    deleted boolean NOT NULL,
    updated_at timestamp without time zone,
    content_pattern character varying(255) NOT NULL,
    organization_id bigint NOT NULL,
    title character varying(255) NOT NULL
);


ALTER TABLE public.message_template OWNER TO postgres;

--
-- Name: message_template_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.message_template_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.message_template_id_seq OWNER TO postgres;

--
-- Name: message_template_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.message_template_id_seq OWNED BY public.message_template.id;


--
-- Name: transaction; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.transaction (
    amount bigint DEFAULT 0 NOT NULL,
    client_id bigint NOT NULL,
    client_transaction_id bigint NOT NULL,
    created_at timestamp without time zone DEFAULT now(),
    from_user_id bigint NOT NULL,
    id bigint NOT NULL,
    service_id bigint NOT NULL,
    to_user_id bigint NOT NULL,
    updated_at timestamp(6) without time zone,
    status character varying(10),
    generic_params character varying(100000)[],
    CONSTRAINT transaction_status_check CHECK (((status)::text = ANY ((ARRAY['CANCELED'::character varying, 'SUCCESS'::character varying, 'PENDING'::character varying])::text[])))
);


ALTER TABLE public.transaction OWNER TO postgres;

--
-- Name: transaction_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.transaction_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.transaction_id_seq OWNER TO postgres;

--
-- Name: transaction_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.transaction_id_seq OWNED BY public.transaction.id;


--
-- Name: worker_service; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.worker_service (
    id bigint NOT NULL,
    description character varying(255) NOT NULL,
    service_name character varying(255) NOT NULL
);


ALTER TABLE public.worker_service OWNER TO postgres;

--
-- Name: worker_service_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.worker_service_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.worker_service_id_seq OWNER TO postgres;

--
-- Name: worker_service_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.worker_service_id_seq OWNED BY public.worker_service.id;


--
-- Name: auth_user id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.auth_user ALTER COLUMN id SET DEFAULT nextval('public.auth_user_id_seq'::regclass);


--
-- Name: client id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.client ALTER COLUMN id SET DEFAULT nextval('public.client_id_seq'::regclass);


--
-- Name: message_template id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.message_template ALTER COLUMN id SET DEFAULT nextval('public.message_template_id_seq'::regclass);


--
-- Name: transaction id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.transaction ALTER COLUMN id SET DEFAULT nextval('public.transaction_id_seq'::regclass);


--
-- Name: worker_service id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.worker_service ALTER COLUMN id SET DEFAULT nextval('public.worker_service_id_seq'::regclass);


--
-- Data for Name: auth_user; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.auth_user (balance, id, full_name, phone_number, wallet_number) FROM stdin;
\.
COPY public.auth_user (balance, id, full_name, phone_number, wallet_number) FROM '$$PATH$$/3401.dat';

--
-- Data for Name: client; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.client (id, password, username) FROM stdin;
\.
COPY public.client (id, password, username) FROM '$$PATH$$/3403.dat';

--
-- Data for Name: databasechangelog; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.databasechangelog (id, author, filename, dateexecuted, orderexecuted, exectype, md5sum, description, comments, tag, liquibase, contexts, labels, deployment_id) FROM stdin;
\.
COPY public.databasechangelog (id, author, filename, dateexecuted, orderexecuted, exectype, md5sum, description, comments, tag, liquibase, contexts, labels, deployment_id) FROM '$$PATH$$/3409.dat';

--
-- Data for Name: databasechangeloglock; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.databasechangeloglock (id, locked, lockgranted, lockedby) FROM stdin;
\.
COPY public.databasechangeloglock (id, locked, lockgranted, lockedby) FROM '$$PATH$$/3408.dat';

--
-- Data for Name: message_template; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.message_template (id, created_at, created_by, deleted, updated_at, content_pattern, organization_id, title) FROM stdin;
\.
COPY public.message_template (id, created_at, created_by, deleted, updated_at, content_pattern, organization_id, title) FROM '$$PATH$$/3410.dat';

--
-- Data for Name: transaction; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.transaction (amount, client_id, client_transaction_id, created_at, from_user_id, id, service_id, to_user_id, updated_at, status, generic_params) FROM stdin;
\.
COPY public.transaction (amount, client_id, client_transaction_id, created_at, from_user_id, id, service_id, to_user_id, updated_at, status, generic_params) FROM '$$PATH$$/3405.dat';

--
-- Data for Name: worker_service; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.worker_service (id, description, service_name) FROM stdin;
\.
COPY public.worker_service (id, description, service_name) FROM '$$PATH$$/3407.dat';

--
-- Name: auth_user_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.auth_user_id_seq', 6, true);


--
-- Name: client_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.client_id_seq', 10, true);


--
-- Name: message_template_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.message_template_id_seq', 4, true);


--
-- Name: transaction_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.transaction_id_seq', 6, true);


--
-- Name: worker_service_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.worker_service_id_seq', 6, true);


--
-- Name: auth_user auth_user_phone_number_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.auth_user
    ADD CONSTRAINT auth_user_phone_number_key UNIQUE (phone_number);


--
-- Name: auth_user auth_user_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.auth_user
    ADD CONSTRAINT auth_user_pkey PRIMARY KEY (id);


--
-- Name: auth_user auth_user_wallet_number_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.auth_user
    ADD CONSTRAINT auth_user_wallet_number_key UNIQUE (wallet_number);


--
-- Name: client client_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.client
    ADD CONSTRAINT client_pkey PRIMARY KEY (id);


--
-- Name: client client_username_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.client
    ADD CONSTRAINT client_username_key UNIQUE (username);


--
-- Name: databasechangeloglock databasechangeloglock_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.databasechangeloglock
    ADD CONSTRAINT databasechangeloglock_pkey PRIMARY KEY (id);


--
-- Name: message_template message_template_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.message_template
    ADD CONSTRAINT message_template_pkey PRIMARY KEY (id);


--
-- Name: transaction transaction_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.transaction
    ADD CONSTRAINT transaction_pkey PRIMARY KEY (id);


--
-- Name: worker_service worker_service_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.worker_service
    ADD CONSTRAINT worker_service_pkey PRIMARY KEY (id);


--
-- PostgreSQL database dump complete
--

                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               