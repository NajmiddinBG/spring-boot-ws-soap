package uz.friday.sqb_task.utils;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import jakarta.persistence.AttributeConverter;
import lombok.extern.slf4j.Slf4j;
import uz.friday.sqb_task.dto.GenericParam;

@Slf4j
public class GenericParamAttributeConverter implements AttributeConverter<GenericParam, String> {
    private static final ObjectMapper objectMapper = new ObjectMapper();

    @Override
    public String convertToDatabaseColumn(GenericParam genericParam) {
        try {
            return objectMapper.writeValueAsString(genericParam);
        } catch (JsonProcessingException jpe) {
            log.info("Cannot convert Address into JSON");
            return null;
        }
    }

    @Override
    public GenericParam convertToEntityAttribute(String s) {
        try {
            return objectMapper.readValue(s, GenericParam.class);
        } catch (JsonProcessingException e) {
            log.info("Cannot convert JSON into Address");
            return null;
        }
    }
}
