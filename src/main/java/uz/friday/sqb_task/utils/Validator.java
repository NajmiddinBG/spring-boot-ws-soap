package uz.friday.sqb_task.utils;

import org.springframework.stereotype.Component;

import java.util.Objects;

@Component
public class Validator {

    public static boolean isConvertibleToLong(String str) {
        if (Objects.isNull(str)) return false;
        try {
            Long.parseLong(str);
            return true;
        } catch (NumberFormatException e) {
            return false;
        }
    }

}
