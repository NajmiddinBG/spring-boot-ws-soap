package uz.friday.sqb_task.entity;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import uz.friday.sqb_task.entity.base.BaseAuditable;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class WorkerService extends BaseAuditable {

    @Column(nullable = false)
    private String serviceName;

    @Column(nullable = false)
    private String description;

}
