package uz.friday.sqb_task.entity;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import uz.friday.sqb_task.entity.base.BaseAuditable;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class AuthUser extends BaseAuditable {

    @Column(nullable = false, unique = true)
    private String phoneNumber;

    @Column(nullable = false, unique = true)
    private String walletNumber;

    @Column(nullable = false)
    private String fullName;

    @Column(nullable = false, columnDefinition = "BIGINT default 0")
    private Long balance;
}
