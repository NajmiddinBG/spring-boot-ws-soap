package uz.friday.sqb_task.entity;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import uz.friday.sqb_task.dto.GenericParam;
import uz.friday.sqb_task.entity.base.Auditable;
import uz.friday.sqb_task.enums.TransactionStatus;
import uz.friday.sqb_task.utils.GenericParamAttributeConverter;

import java.util.List;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Transaction extends Auditable {

    @Column(nullable = false)
    private Long serviceId;

    @Column(nullable = false)
    private Long fromUserId;

    @Column(nullable = false)
    private Long toUserId;

    @Column(nullable = false)
    private Long clientTransactionId;

    @Column(nullable = false)
    private Long clientId;

    @Column(nullable = false, columnDefinition = "BIGINT default 0")
    private Long amount;

    @Column(length = 10)
    @Enumerated(EnumType.STRING)
    private TransactionStatus status;

    @Convert(converter = GenericParamAttributeConverter.class)
    @Column(name = "generic_params", length = 100_000)
    private List<GenericParam> genericParams;

}
