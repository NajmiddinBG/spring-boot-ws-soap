package uz.friday.sqb_task.annotations;

import jakarta.validation.Constraint;
import jakarta.validation.Payload;
import uz.friday.sqb_task.annotations.validators.CheckingAmountValidator;
import uz.friday.sqb_task.annotations.validators.HaveServiceValidator;

import java.lang.annotation.*;

@Target({ElementType.FIELD, ElementType.PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Constraint(validatedBy = CheckingAmountValidator.class)
public @interface CheckingAmount {

    String message() default "amount value is not valid";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};

}
