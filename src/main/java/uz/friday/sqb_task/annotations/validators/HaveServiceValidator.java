package uz.friday.sqb_task.annotations.validators;

import jakarta.validation.ConstraintValidator;
import jakarta.validation.ConstraintValidatorContext;
import org.springframework.stereotype.Component;
import uz.friday.sqb_task.annotations.HaveService;
import uz.friday.sqb_task.service.ProviderWebService;

@Component
public class HaveServiceValidator implements ConstraintValidator<HaveService, Long> {

    private final ProviderWebService providerWebService;

    public HaveServiceValidator(ProviderWebService providerWebService) {
        this.providerWebService = providerWebService;
    }

    @Override
    public boolean isValid(Long serviceId, ConstraintValidatorContext constraintValidatorContext) {
        return providerWebService.isHaveService(serviceId);
    }
}
