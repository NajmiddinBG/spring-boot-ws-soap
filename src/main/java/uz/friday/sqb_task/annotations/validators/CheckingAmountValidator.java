package uz.friday.sqb_task.annotations.validators;

import jakarta.validation.ConstraintValidator;
import jakarta.validation.ConstraintValidatorContext;
import org.springframework.stereotype.Component;
import uz.friday.sqb_task.annotations.CheckingAmount;

import java.util.Objects;

@Component
public class CheckingAmountValidator implements ConstraintValidator<CheckingAmount, Long> {
    @Override
    public boolean isValid(Long amount, ConstraintValidatorContext constraintValidatorContext) {
        return Objects.nonNull(amount) && amount > 0 && amount <= 124500000;
    }
}
