package uz.friday.sqb_task.annotations;

import jakarta.validation.Constraint;
import jakarta.validation.Payload;
import uz.friday.sqb_task.annotations.validators.HaveServiceValidator;

import java.lang.annotation.*;

@Target({ElementType.FIELD, ElementType.PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Constraint(validatedBy = HaveServiceValidator.class)
public @interface HaveService {

    String message() default "Service is not found";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};

}
