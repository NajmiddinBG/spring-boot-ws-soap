package uz.friday.sqb_task.endpoint;

import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;
import uz.friday.sqb_task.dto.*;
import uz.friday.sqb_task.service.ProviderWebService;

@Endpoint
@Slf4j
@Validated
@RequiredArgsConstructor
public class ProviderWebServiceEndpoint {

    private static final String NAMESPACE_URI = "http://uws.provider.com/";


    private final ProviderWebService providerWebService;

    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "PerformTransactionRequest")
    @ResponsePayload
    public PerformTransactionResponse PerformTransaction(@RequestPayload @Valid PerformTransactionRequest request) {
        return providerWebService.performTransaction(request);
    }

    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "GetInformationRequest")
    @ResponsePayload
    public GetInformationResponse GetInformation(@RequestPayload @Valid GetInformationRequest request) {
        return providerWebService.getInformation(request);
    }

    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "CheckTransactionRequest")
    @ResponsePayload
    public CheckTransactionResponse CheckTransaction(@RequestPayload @Valid CheckTransactionRequest request) {
        return providerWebService.checkTransaction(request);
    }

    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "CancelTransactionArguments")
    @ResponsePayload
    public CancelTransactionResponse CancelTransaction(@RequestPayload @Valid CancelTransactionRequest request) {
        return providerWebService.cancelTransaction(request);
    }

    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "GetStatementRequest")
    @ResponsePayload
    public GetStatementResponse GetStatement(@RequestPayload @Valid GetStatementRequest request) {
        return providerWebService.getStatement(request);
    }

    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "ChangePasswordRequest")
    @ResponsePayload
    public ChangePasswordResponse ChangePassword(@RequestPayload @Valid ChangePasswordRequest request) {
        return providerWebService.changePassword(request);
    }
}
