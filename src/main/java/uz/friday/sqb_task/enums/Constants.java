package uz.friday.sqb_task.enums;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class Constants {

    public static final String OK = "Ok";
    public static final String CLIENT_NOT_FOUND_BY_USERNAME_MESSAGE = "Client not found by Username/Password";
    public static final String CUSTOM_USER_NOT_FOUND_MESSAGE = "Customer user not found parameters or customer_id invalid";
    public static final String USER_NOT_FOUND_BY_CARD_NUMBER_PHONE_NUMBER = "User walled not found by phoneNumber/cardNumber";
    public static final String USER_NOT_FOUND = "User not found";
    public static final String USER_BALANCE_ERROR = "User does not have sufficient withdrawal sums";
    public static final String CLIENT_ID_FIELD_INVALID = "client_id field invalid";
}
