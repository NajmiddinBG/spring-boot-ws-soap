package uz.friday.sqb_task.enums;

import lombok.Getter;

@Getter
public enum TransactionStatus {
    CANCELED,
    SUCCESS
}
