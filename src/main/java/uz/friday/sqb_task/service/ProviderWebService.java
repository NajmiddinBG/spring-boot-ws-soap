package uz.friday.sqb_task.service;


//import com.provider.uws.*;
import uz.friday.sqb_task.dto.*;
import uz.friday.sqb_task.entity.WorkerService;

import java.util.Optional;

public interface ProviderWebService {

    PerformTransactionResponse performTransaction(PerformTransactionRequest request);

    CheckTransactionResponse checkTransaction(CheckTransactionRequest request);

    CancelTransactionResponse cancelTransaction(CancelTransactionRequest request);

    GetStatementResponse getStatement(GetStatementRequest request);

    GetInformationResponse getInformation(GetInformationRequest request);

    ChangePasswordResponse changePassword(ChangePasswordRequest request);

    Boolean isHaveService(Long serviceId);

}
