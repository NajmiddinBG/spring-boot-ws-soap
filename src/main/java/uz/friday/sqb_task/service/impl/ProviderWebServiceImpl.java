package uz.friday.sqb_task.service.impl;

//import com.provider.uws.*;

import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import uz.friday.sqb_task.dto.*;
import uz.friday.sqb_task.entity.AuthUser;
import uz.friday.sqb_task.entity.Client;
import uz.friday.sqb_task.entity.Transaction;
import uz.friday.sqb_task.enums.Constants;
import uz.friday.sqb_task.enums.TransactionStatus;
import uz.friday.sqb_task.repository.AuthUserRepository;
import uz.friday.sqb_task.repository.ClientRepository;
import uz.friday.sqb_task.repository.TransactionRepository;
import uz.friday.sqb_task.repository.WorkerServiceRepository;
import uz.friday.sqb_task.service.ProviderWebService;
import uz.friday.sqb_task.utils.Validator;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

@Slf4j
@Service
@RequiredArgsConstructor
public class ProviderWebServiceImpl implements ProviderWebService {

    private final WorkerServiceRepository serviceRepository;
    private final ClientRepository clientRepository;
    private final AuthUserRepository authUserRepository;
    private final TransactionRepository transactionRepository;

    @Override
    @Transactional
    public PerformTransactionResponse performTransaction(PerformTransactionRequest request) {

        Optional<Client> optionalClient = clientRepository.findByUsernameAndPassword(request.getUsername(), request.getPassword());
        if (optionalClient.isEmpty())
            return createErrorForPerformResponse(Constants.CLIENT_NOT_FOUND_BY_USERNAME_MESSAGE, 1, LocalDateTime.now()); //This is error

        if (Objects.isNull(request.getTransactionTime())) request.setTransactionTime(LocalDateTime.now());

        Optional<AuthUser> optionalToUser;
        if (Validator.isConvertibleToLong(getParameterValue(request.getParameters(), "customer_id"))) {
            optionalToUser = authUserRepository.findById(Long.parseLong(Objects.requireNonNull(getParameterValue(request.getParameters(), "customer_id"))));
        } else
            return createErrorForPerformResponse(Constants.CUSTOM_USER_NOT_FOUND_MESSAGE, 1, LocalDateTime.now()); //This is error

        String fromUser = getParameterValue(request.getParameters(), "pin");
        Optional<AuthUser> optionalFromUser;
        if (isPhoneNumber(fromUser)) {
            optionalFromUser = authUserRepository.findByPhoneNumber(fromUser);
        } else if (isCardNumber(fromUser)) {
            optionalFromUser = authUserRepository.findByWalletNumber(fromUser);
        } else
            return createErrorForPerformResponse(Constants.USER_NOT_FOUND_BY_CARD_NUMBER_PHONE_NUMBER, 1, LocalDateTime.now());

        if (optionalToUser.isEmpty() || optionalFromUser.isEmpty())
            return createErrorForPerformResponse(Constants.USER_NOT_FOUND, 1, LocalDateTime.now()); //This is error

        AuthUser fromAuthUser = optionalFromUser.get();
        AuthUser toAuthUser = optionalToUser.get();
        if (request.getAmount() > fromAuthUser.getBalance())
            return createErrorForPerformResponse(Constants.USER_BALANCE_ERROR, 1, LocalDateTime.now()); //This is error

        Transaction transaction = new Transaction();
        transaction.setServiceId(request.getServiceId());
        transaction.setAmount(request.getAmount());
        transaction.setClientTransactionId(request.getTransactionId());
        transaction.setClientId(optionalClient.get().getId());
        transaction.setFromUserId(fromAuthUser.getId());
        transaction.setToUserId(toAuthUser.getId());
        transaction.setGenericParams(request.getParameters());


        Long newToUserAmount = toAuthUser.getBalance() + request.getAmount();
        Long newFromUserAmount = fromAuthUser.getBalance() - request.getAmount();
        toAuthUser.setBalance(newToUserAmount);
        fromAuthUser.setBalance(newFromUserAmount);
        AuthUser savedFromUser = authUserRepository.save(fromAuthUser);
        authUserRepository.save(toAuthUser);
        transaction.setStatus(TransactionStatus.SUCCESS);
        Transaction savedTransaction = transactionRepository.save(transaction);

        PerformTransactionResponse response = new PerformTransactionResponse();
        response.setStatus(0);
        response.setErrorMsg(Constants.OK);
        response.setProviderTrnId(savedTransaction.getId());
        response.setTimeStamp(LocalDateTime.now());
        response.setParameters(
                List.of(
                        new GenericParam("balance", savedFromUser.getBalance().toString()),
                        new GenericParam("traffic", "?"),
                        new GenericParam("timestamp", LocalDateTime.now().format(DateTimeFormatter.ofPattern("dd.MM.yyyy HH:mm:ss")))
                ));
        return response;
    }

    @Override
    public CheckTransactionResponse checkTransaction(CheckTransactionRequest request) {
        CheckTransactionResponse response = new CheckTransactionResponse();
        response.setStatus(0);
        response.setErrorMsg(Constants.OK);
        response.setTimeStamp(LocalDateTime.now());
        return response;
    }

    @Override
    public CancelTransactionResponse cancelTransaction(CancelTransactionRequest request) {
        CancelTransactionResponse response = new CancelTransactionResponse();
        response.setStatus(0);
        response.setErrorMsg(Constants.OK);
        response.setTimeStamp(LocalDateTime.now());
        return response;
    }

    @Override
    public GetStatementResponse getStatement(GetStatementRequest request) {
        Optional<Client> optionalClient = clientRepository.findByUsernameAndPassword(request.getUsername(), request.getPassword());
        if (optionalClient.isEmpty())
            return createErrorForGetStatementResponse(Constants.CLIENT_NOT_FOUND_BY_USERNAME_MESSAGE, 1, LocalDateTime.now());
        List<Transaction> transactions = transactionRepository.findAllByServiceIdAndFromDateAndToDate(request.getServiceId(), optionalClient.get().getId(), request.getDateFrom(), request.getDateTo());

        List<TransactionStatement> statements = transactions.stream().map(t -> new TransactionStatement(t.getAmount(), t.getId(), t.getClientTransactionId(), t.getCreatedAt())).toList();

        GetStatementResponse response = new GetStatementResponse();
        response.setTimeStamp(LocalDateTime.now());
        response.setStatus(0);
        response.setErrorMsg(Constants.OK);
        response.setStatements(statements);
        return response;
    }

    @Override
    public GetInformationResponse getInformation(GetInformationRequest request) {

        Optional<Client> optionalClient = clientRepository.findByUsernameAndPassword(request.getUsername(), request.getPassword());
        if (optionalClient.isEmpty())
            return createErrorGetInformationResponse(Constants.CLIENT_NOT_FOUND_BY_USERNAME_MESSAGE, 1, LocalDateTime.now()); //This is error

        String clientId = getParameterValue(request.getParameters(), "client_id");
        if (!Validator.isConvertibleToLong(clientId))
            return createErrorGetInformationResponse(Constants.CLIENT_ID_FIELD_INVALID, 2, LocalDateTime.now());

        Optional<AuthUser> authUser = authUserRepository.findById(Long.parseLong(clientId));
        if (authUser.isEmpty())
            return createErrorGetInformationResponse(Constants.USER_NOT_FOUND, 2, LocalDateTime.now());

        GetInformationResponse response = new GetInformationResponse();
        response.setTimeStamp(LocalDateTime.now());
        response.setStatus(0);
        response.setErrorMsg(Constants.OK);
        response.setParameters(
                List.of(
                        new GenericParam("balance", authUser.get().getBalance().toString()),
                        new GenericParam("name", authUser.get().getFullName())
                ));

        return response;
    }

    @Override
    public ChangePasswordResponse changePassword(ChangePasswordRequest request) {
        Optional<Client> optionalClient = clientRepository.findByUsernameAndPassword(request.getUsername(), request.getPassword());
        if (optionalClient.isEmpty())
            return createErrorForChangePasswordResponse(Constants.CLIENT_NOT_FOUND_BY_USERNAME_MESSAGE, 2, LocalDateTime.now());

        Client client = optionalClient.get();
        client.setPassword(request.getNewPassword());
        clientRepository.save(client);
        ChangePasswordResponse response = new ChangePasswordResponse();
        response.setErrorMsg(Constants.OK);
        response.setStatus(0);
        response.setTimeStamp(LocalDateTime.now());
        return response;
    }

    @Override
    public Boolean isHaveService(Long serviceId) {
        return serviceRepository.findById(serviceId).isPresent();
    }

    private String getParameterValue(List<GenericParam> parameters, String key) {
        for (GenericParam parameter : parameters) {
            if (parameter.getParamKey().equals(key))
                return parameter.getParamValue();
        }
        return null;
    }

    private boolean isPhoneNumber(String text) {
        return (Objects.nonNull(text) && text.length() == 13 && Validator.isConvertibleToLong(text.substring(1)));
    }

    private boolean isCardNumber(String text) {
        return (Objects.nonNull(text) && text.length() == 16 && Validator.isConvertibleToLong(text));
    }

    private GetStatementResponse createErrorForGetStatementResponse(String errorMessage, int status, LocalDateTime timestamp) {
        GetStatementResponse response = new GetStatementResponse();
        response.setStatements(Collections.emptyList());
        response.setErrorMsg(errorMessage);
        response.setStatus(status);
        response.setTimeStamp(timestamp);
        return response;
    }

    private GetInformationResponse createErrorGetInformationResponse(String errorMessage, int status, LocalDateTime timestamp) {
        GetInformationResponse response = new GetInformationResponse();
        response.setErrorMsg(errorMessage);
        response.setStatus(status);
        response.setTimeStamp(timestamp);
        return response;
    }

    private PerformTransactionResponse createErrorForPerformResponse(String errorMessage, Integer status, LocalDateTime timestamp) {
        PerformTransactionResponse response = new PerformTransactionResponse();
        response.setErrorMsg(errorMessage);
        response.setStatus(status);
        response.setTimeStamp(timestamp);
        return response;
    }

    private ChangePasswordResponse createErrorForChangePasswordResponse(String errorMessage, int status, LocalDateTime timestamp) {
        ChangePasswordResponse response = new ChangePasswordResponse();
        response.setErrorMsg(errorMessage);
        response.setStatus(status);
        response.setTimeStamp(timestamp);
        return response;
    }


}
