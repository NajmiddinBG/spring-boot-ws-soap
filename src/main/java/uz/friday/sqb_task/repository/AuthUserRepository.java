package uz.friday.sqb_task.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import uz.friday.sqb_task.entity.AuthUser;

import java.util.Optional;

public interface AuthUserRepository extends JpaRepository<AuthUser, Long> {

    Optional<AuthUser> findByPhoneNumber(String fromUser);

    Optional<AuthUser> findByWalletNumber(String fromUser);
}
