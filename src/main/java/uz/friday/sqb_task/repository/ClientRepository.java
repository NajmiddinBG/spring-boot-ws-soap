package uz.friday.sqb_task.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import uz.friday.sqb_task.entity.Client;

import java.util.Optional;

public interface ClientRepository extends JpaRepository<Client, Long> {
    Optional<Client> findByUsernameAndPassword(String username, String password);
}
