package uz.friday.sqb_task.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import uz.friday.sqb_task.entity.Transaction;

import java.time.LocalDateTime;
import java.util.List;

public interface TransactionRepository extends JpaRepository<Transaction, Long> {

    @Query("""
            SELECT tr FROM Transaction tr WHERE tr.clientId = :id and tr.serviceId = :serviceId and tr.createdAt BETWEEN :dateFrom AND :dateTo
            """)
    List<Transaction> findAllByServiceIdAndFromDateAndToDate(Long serviceId, Long id, LocalDateTime dateFrom, LocalDateTime dateTo);
}
