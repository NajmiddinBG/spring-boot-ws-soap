package uz.friday.sqb_task.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import uz.friday.sqb_task.entity.WorkerService;

public interface WorkerServiceRepository extends JpaRepository<WorkerService, Long> {
}
