insert into worker_service (description, service_name)
values ('First service', 'Uz-1'),
       ('Second service', 'Uz-2'),
       ('Third service', 'Uz-3');

insert into client (password, username)
values ('psw', 'SQB-App'),
       ('psw', 'Uzum-App'),
       ('psw', 'Ipoteka-App'),
       ('psw', 'Realpay-App'),
       ('psw', 'TBC-App');

insert into auth_user (phone_number, full_name, wallet_number, balance)
values ('+998996673203', 'Najmiddin Botirov', '9860000000000000', 120000000000),
       ('+998996673204', 'Abdulla Oripov', '9860000000000001', 120000000000),
       ('+998996673205', 'Pushkin A.S', '9860000000000002', 120000000000),
       ('+998996673206', 'Jony Doe', '9860000000000003', 120000000000),
       ('+998996673207', 'Javohir Maxmanazarov', '9860000000000004', 120000000000),
       ('+998996673208', 'XXX X.X.', '9860000000000005', 120000000000);