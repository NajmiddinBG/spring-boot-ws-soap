# SQB task

### Task uchun documentatsiya

#### Proyektda ishlatilgan texnologiyalar haqida qisqacha ma'lumot:
<p>Proyekt Spring-boot WS bilan ko'tarilgan. Ma'lumotlar almashish SOAP pattern bilan amalga oshiriladi</p>
<p>Vazifaga biriktirilgan xsd filega ko'ra
<code>jaxb2-maven-plugin</code> dan foydalangan holda xsd filega moslashtirib classlar yaratib olingan</p>
bular:

* [Quyidagi manzilda joylashgan](src/main/java/com/provider/uws)
bu classlardagi ma'lumotlarni moslashtirish va strukturaga birlashtirish uchun
* [dto package ichiga ko'chirib o'tqazilgan va fieldlar proyektga moslashtirilgan](src/main/java/uz/friday/sqb_task/dto).

dto sifatida kirib keluvchi requestlarning ba'zi fieldlaridagi qiymatlar 
* [Custom annotatsiyalar](src/main/java/uz/friday/sqb_task/annotations) yoki <code>spring-boot-starter-validation</code> yordamida tekshirilgan.
<p>Database bilan ishlash va unga ma'lumotlarni saqlash, o'chirish, olish uchun PostgreSQL dan foydalanilgan. PostgreSQL bilan 
ulanish uchun DataJPAdan foydalanilgan. Proyektda sodir etilgan exceptionlarni/eventlarni saqlash uchun loglar filega yoziladigan qilib
configuratsiya qilingan(Sl4g). Databaseda proyekt run bo'lgan paytda zarur initial ma'lumotlar kerak bo'lgani uchun migrationdan foydalanilgan
<code>liquibase</code>. </p> 

### Proyektni run qilish uchun e'tibor berilishi kerak bo'lgan configuratsiyalar
1. Proyekt 8888 portda run bo'ladi. Bu band emasligiga ishonch hosil qiling
2. <code>application-dev.yml</code> fileda luqibase configuratsiyasi hamda dataJpa configuratsiyasiga 
etibor qarating. Birinchi martta run qilinayotganda jpa tablelarni yaratmagan bo'ladi. Avval liquibase configda o'chirib
qo'yib jpaning configini create ga o'zgartirib databaseda tablelarni shakllantirib olish kerak. Keyin jpani none qilib qo'yib
liquibasening configuratsiyasini yoqish mumkin bo'ladi.
3. Proyektni test qilish uchun SOAP-UI dan foydalanish tavsiya etiladi.
4. Local Computerda run qilingan proyekt http://localhost:8888/ws/ProviderWebService.wsdl ni generatsiya qiladi. Har bir
Endpoint ichidagi methodlarni test qilib olish mumkin bo'ladi.


